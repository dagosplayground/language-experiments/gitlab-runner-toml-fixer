#!/bin/bash

## Takes information in and converts it to a toml file. 
input_file="data.json"
output_file="data.toml"
other_tags=$1

jq --arg other_tags $other_tags \
  '.
  | select(.)
  | .runners[0].tags=$other_tags 
  ' \
  $input_file -r \
  | yq "." --toml-output \
  | awk '{print "\t " $0}' \
  | tee $output_file

cat << EOL > dataother.toml
    [[runners]]
      name = "eks-my-runner"
      url = "https://personal.gitlab.com"
      clone_url = "https://personal-clone.gitlabl.com"
      tags = "${other_tags}"
      [runners.kubernetes]
        namespace = "{{.Release.Namespace}}"
        image = "artifactory.personal.com/muu-gitlab-images"
        helper_image = "artifactory.personal.com/muu-gitlab-images"
EOL


