# GitLab EKS Runner TOML writer

So... GitLab has this annoying use of using TOML for the runners. While not an issue with single runner deployments, the need has come up for creating a mutli-namespace deployment makes yaml + toml + helm deployments... painful. 

This repository seeks to come up with a solution that addresses these issues as there is an inability to address the root issue with helm where `totoml` will try to convert integers to floats. 

## Steps

The goals are simple: 
1. Get a TOML target 
1. Create a JSON file that can match the TOML target
1. Create a script to edit the JSON using `jq`.
1. Output the updated JSON  
1. Convert that output JSON to TOML and save

**Note:** The end result should be similar to the structure found in `gl.toml`. 

## Example

The script should take in an argument to set the tags dynamically in the json, the output should be properly formatted Toml with all the changes. 

```bash
./toml_builder.sh "eks,muu,k8s"
```

*Output can be found in `data.toml`.*

### Input 
```json
{
  "runners": [
    {
      "name": "eks-my-runner",
      "url": "https://personal.gitlab.com",
      "clone_url": "https://personal-clone.gitlabl.com",
      "tags": "eks,csbu,sample,taco",
      "kubernetes": {
        "namespace": "{{.Release.Namespace}}",
        "image": "artifactory.personal.com/muu-gitlab-images",
        "helper_image": "artifactory.personal.com/muu-gitlab-images"
      }
    }
  ]
}
```

### Output
```toml
[[runners]]
name = "eks-my-runner"
url = "https://personal.gitlab.com"
clone_url = "https://personal-clone.gitlabl.com"
tags = "eks,muu,k8s"

[runners.kubernetes]
namespace = "{{.Release.Namespace}}"
image = "artifactory.personal.com/muu-gitlab-images"
helper_image = "artifactory.personal.com/muu-gitlab-images"
```

